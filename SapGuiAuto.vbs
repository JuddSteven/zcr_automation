'-Begin-----------------------------------------------------------------

Option Explicit

Dim SapGuiAuto, application, connection, session

If Not IsObject(application) Then
  Set SapGuiAuto = GetObject("SAPGUI")
  Set application = SapGuiAuto.GetScriptingEngine
End If

If Not IsObject(connection) Then
  Set connection = application.Children(CInt(0))
End If

If Not IsObject(session) Then
  Set session = connection.Children(CInt(0))
End If

session.findById("wnd[0]").maximize
session.findById("wnd[0]/tbar[0]/okcd").text = "/n/rad/dwl"
session.findById("wnd[0]/tbar[0]/btn[0]").press
session.findById("wnd[0]/usr/cntlCLFRM_CONTAINER/shellcont/shell/shellcont/shell/shellcont[1]/shell/shellcont/shell").modifyCell 5,"VALUE1","72000580"
session.findById("wnd[0]/usr/cntlCLFRM_CONTAINER/shellcont/shell/shellcont/shell/shellcont[1]/shell/shellcont/shell").currentCellRow = 5
session.findById("wnd[0]/usr/cntlCLFRM_CONTAINER/shellcont/shell/shellcont/shell/shellcont[1]/shell/shellcont/shell").firstVisibleColumn = "LABEL1"
session.findById("wnd[0]/tbar[1]/btn[8]").press
session.findById("wnd[0]/usr/cntlCLFRM_CONTAINER/shellcont/shell/shellcont/shell/shellcont[2]/shell/shellcont[2]/shell").doubleClickCurrentCell
session.findById("wnd[0]/usr/ssubMAIN_SCR:/RAD/SAPLDMF_MAIN_DB:1021/ssubSUB_ACTIONS:/RAD/SAPLDMF_MAIN_DB:0031/subSUB_ACTIONS2:/RAD/SAPLDMF_MAIN_DB:0033/tabsACTIONS_CASE_OVERVIEW/tabpT\34/ssubACTIONS_CASE:/RAD/SAPLDMF_MAIN_DB:0084/cntlMYACTIONS/shellcont/shell").setCurrentCell 2,"ACTION"
session.findById("wnd[0]/usr/ssubMAIN_SCR:/RAD/SAPLDMF_MAIN_DB:1021/ssubSUB_ACTIONS:/RAD/SAPLDMF_MAIN_DB:0031/subSUB_ACTIONS2:/RAD/SAPLDMF_MAIN_DB:0033/tabsACTIONS_CASE_OVERVIEW/tabpT\34/ssubACTIONS_CASE:/RAD/SAPLDMF_MAIN_DB:0084/cntlMYACTIONS/shellcont/shell").clickCurrentCell
session.findById("wnd[0]/usr/cntlCLFRM_CONTAINER/shellcont/shell/shellcont[1]/shell/shellcont/shell/shellcont[1]/shell/shellcont/shell").firstVisibleColumn = "LABEL1"
session.findById("wnd[0]/usr/cntlCLFRM_CONTAINER/shellcont/shell/shellcont[1]/shell/shellcont/shell/shellcont[0]/shell").pressButton "TOGGLE_DISPLAY_CHANGE"
session.findById("wnd[0]/usr/cntlCLFRM_CONTAINER/shellcont/shell/shellcont[1]/shell/shellcont/shell/shellcont[1]/shell/shellcont/shell").modifyCell 1,"VALUE1","Customer Pass Thru Claim"
session.findById("wnd[0]/usr/cntlCLFRM_CONTAINER/shellcont/shell/shellcont[1]/shell/shellcont/shell/shellcont[0]/shell").pressButton "SAVE"
session.findById("wnd[0]/tbar[0]/btn[3]").press

'-End-------------------------------------------------------------------