'-Begin-----------------------------------------------------------------

Option Explicit

Dim SapGuiAuto, application, connection, session

If Not IsObject(application) Then
  Set SapGuiAuto = GetObject("SAPGUI")
  Set application = SapGuiAuto.GetScriptingEngine
End If

If Not IsObject(connection) Then
  Set connection = application.Children(CInt(0))
End If

If Not IsObject(session) Then
  Set session = connection.Children(CInt(0))
End If


session.findById("wnd[1]/usr/cmbROLES").key = "NLR007"
session.findById("wnd[1]/usr/cntlCTRL_1000/shellcont/shell").selectedRows = "17,18,20,21"
session.findById("wnd[1]/usr/btnFORWARD").press
session.findById("wnd[2]/tbar[0]/btn[0]").press
