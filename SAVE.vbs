'-Begin-----------------------------------------------------------------

Option Explicit

Dim SapGuiAuto, application, connection, session

If Not IsObject(application) Then
  Set SapGuiAuto = GetObject("SAPGUI")
  Set application = SapGuiAuto.GetScriptingEngine
End If

If Not IsObject(connection) Then
  Set connection = application.Children(CInt(0))
End If

If Not IsObject(session) Then
  Set session = connection.Children(CInt(0))
End If

session.findById("wnd[0]").maximize
session.findById("wnd[0]/usr/cntlCLFRM_CONTAINER/shellcont/shell/shellcont[1]/shell/shellcont/shell/shellcont[0]/shell").pressButton "SAVE"

'-End-------------------------------------------------------------------